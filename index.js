// console.log("Go Sir Hernan!")

// [FETCH ()]
	/*
		- a method in js that is used to send request in the server and load the information (response from the server) in the webpages.
	
		- syntax:
			fetch ("urlAPI", {optional/ req from the user & res to the user} )
	*/


// let specificPost = (id) => {

// 	if(id <= 100 && id >= 1){
// 		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
// 		.then(response => response.json())
// 		.then(data => console.log(data))
// 	}
// }

// specificPost(5)

//fetching all the elements in the db


let fetchPosts = () => {
	fetch("https://jsonplaceholder.typicode.com/posts").then(response => response.json())
	//para hindi promise ang ireturn ulit
	.then(data => {console.log(data);
	return showPosts(data)}); 
}

fetchPosts();


const showPosts = (posts) =>{
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
		<div id = "post-${post.id}">
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onclick = "editPost('${post.id}')" >Edit</button>
			<button onclick = "deletePost('${post.id}')" >Delete</button>
		</div>
		`
	})
	document.querySelector("#div-post-entries").innerHTML =postEntries
}


// Add Post 

	document.querySelector("#form-add-post").addEventListener("submit", (event) => {
		event.preventDefault();

		let title = document.querySelector("#txt-title").value
		let body = document.querySelector("#txt-body").value

		fetch("https://jsonplaceholder.typicode.com/posts/", {
			method: "POST", 
			body: JSON.stringify({
				title : title,
				body: body,
				userId: 1
			}),
			headers: {
				"Content-Type" : "application/json"
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);
			alert("Successfully added!")

			document.querySelector("#txt-title").value = null;
			document.querySelector("#txt-body").value = null;

		})
	})

// Edit post

	const editPost = (id) =>{
		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		document.querySelector('#txt-edit-title').value = title;
		document.querySelector('#txt-edit-body').value = body;
		document.querySelector('#txt-edit-id').value = id;
		//.removeAttribute will remove the attribute from the selected element
		document.querySelector('#btn-submit-update').removeAttribute('disabled')

	}

	document.querySelector("#form-edit-post").addEventListener("submit", (event) =>{
	event.preventDefault();

	let id = document.querySelector('#txt-edit-id').value
	let title = document.querySelector('#txt-edit-title').value
	let body = document.querySelector('#txt-edit-body').value

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
		method : "PATCH",
		body: JSON.stringify({
			title: title,
			body: body,
		}),
		headers: {
			"Content-Type" : "application/json"
		}
	}).then(response => response.json())
	.then(data => {
		console.log(data)
		alert("Successfully updated!");

		document.querySelector('#txt-edit-id').value = null
		document.querySelector('#txt-edit-title').value = null
		document.querySelector('#txt-edit-body').value = null

		document.querySelector('#btn-submit-update').setAttribute('disabled', true)

	})
})

// delete post
	
	const deletePost = async (id) =>{
		// console.log("Hi Im Delete") // to confirm if we can invoke the delete()
		await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
			method: 'DELETE'})
		.then(response => response.json()).then(data => alert("Post successfully deleted!"))

		document.querySelector(`#post-${id}`).remove()
	}

	//async await to delete the post first before the alert